All the commands described in this file must be executed in the src directory of the project.

###########
#Compiling#
###########

make compile

#############
#Quick setup#
#############

When the following command is executed 12 terminals will be opened: 1 network, 1 tracker, 6 nodes, 4 peers. They will all use default values corresponding to a localhost set up. 

make TERMINAL=<Terminal>

By default, <Terminal> is gnome-terminal.

They can all be closed easily with the following command (make sure the src/killall.sh script has the permission to execute on your system).

make kill

##############
#Custom setup# 
##############

Preliminary remark: By default, all the hostnames are 127.0.0.1

Step 1
------ 
The network must be created with the following command

make network NETWORKHOSTNAME=<NetworkHostname> 

The port of the network is always 8080.

Step 2
------
The tracker must be created with the following command

make tracker NETWORKHOSTNAME=<NetworkHostname> TRACKERHOSTNAME=<TrackerHostname> TRACKERPORT=<TrackerPort>

By default, the port of the tracker is 8087.

Step 3
------
The nodes must be created with the following command

make node NETWORKHOSTNAME=<NetworkHostname> NODEHOSTNAME=<NodeHostname> NODEPORT=<NodePort>

By default, the port of a node is 8081. Note that there must be at least 4 nodes on the network.

Step 4
------
The peers must be created with the following command

make peer NETWORKHOSTNAME=<NetworkHostname> PEERCLIENTDIRECTORY=<PeerClientDirectory> PEERSERVERDIRECTORY=<PeerServerDirectory>

<PeerClientDirectory> is the destination directory of the downloaded files. <PeerServerDirectory> is the directory of the available files of a peer. Note that only the files of <PeerServerDirectory> will be made available, not its directories.

#######
#Usage#
#######

A peer wanting to download a file must first press enter. That will display the list of available files on the tracker. Then, the peer can either return to the previous screen by pressing enter or select a file by entering its name or its number in the list. That will start the download and display the download percentage of the file. Once the download is finished, the peer returns to the initial situation in which he must press enter to display the list of available files.
