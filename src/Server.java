import java.io.IOException;
import java.net.Socket;
import java.net.ServerSocket;
import java.net.SocketAddress;

/**
 * Class representing a server
 */
abstract public class Server {

    /**
     * Socket of the server to receive entering connections
     */
    private ServerSocket server;

    /**
     * Constructor
     * @param address address of the server socket
     * @throws IOException
     */
    public Server(SocketAddress address) throws IOException {
        server = new ServerSocket();
        server.bind(address);
    }

    /**
     * Listen for new requests through the server's socket. Each request is executed by a new thread.
     */
    public void listen() {
        while(true) {
            try {
                Socket socket = server.accept();
                new Thread(() -> answerTheRequest(socket)).start();
            }
            catch(IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Answers to a request made by an entity connected to the server
     * @param requestSocket entity which made the request
     */
    abstract protected void answerTheRequest(Socket requestSocket);
}
