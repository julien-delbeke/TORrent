import java.math.BigInteger;

/**
 * Class representing a pointer to a BigInteger. Used to keep different entities up to date with the value of a AES128-CTR counter.
 */
public class BigIntegerPointer {

    public BigInteger ctr;

    /**
     * Constructor
     * @param ctr counter
     */
    BigIntegerPointer(BigInteger ctr) {
        this.ctr = ctr;
    }
}
