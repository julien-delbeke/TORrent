import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.InetSocketAddress;

/**
 * Abstract class Implementing a server known by everyone and which notifies the network of its existence when it's created.
 * It has a public key and a private key used to communicate with entities on the network.
 */
abstract public class PublicServer extends Server {

    /**
     * RSAES-OAEP used for encryption
     */
    private RSAES_OAEP rsa;

    /**
     * Constructor
     * @param address network address
     * @throws IOException
     */
    public PublicServer(InetSocketAddress address) throws IOException{
        super(address);
        rsa = new RSAES_OAEP();
        notifyNetwork(address);
    }

    /**
     * Notifies the network of its existence on the network
     * @param address network adress
     * @throws IOException
     */
    private void notifyNetwork(InetSocketAddress address) throws IOException {
        Socket socket = NetworkAddress.getNetworkSocket();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
        objectOutputStream.writeByte(getCaseNotify());
        objectOutputStream.writeObject(address);
        objectOutputStream.writeObject(rsa.getPublicKey());
        objectOutputStream.flush();
        ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
        objectInputStream.readByte();//be sure than the server recive all the information
        objectInputStream.close();
        objectOutputStream.close();
        socket.close();
    }

    /**
     * Returns the RSAES-OAEP instance of the server
     * @return rsa RSAES-OAEP instance
     */
    protected RSAES_OAEP getRSA() {
        return rsa;
    }

    /**
     * Returns the type of the request to the network. See Network class attributes.
     * @return the type of the request
     */
    abstract protected byte getCaseNotify();
}
