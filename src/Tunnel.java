/**
 * Class representing a tunnel between two entities on the network
 */
public class Tunnel {

    /**
     * Socket of the first entity of the tunnel
     */
    private SecureSocket first;

    /**
     * Socket of the second entity of the tunnel
     */
    private SecureSocket second;

    /**
     * Thread for the first entity
     */
    private Thread thread;

    /**
     * Thread for the second entity
     */
    private Thread thread2;

    /**
     * Indicates the state of the connection to ensure that a connection will never be closed several times due to race conditions
     */
    private boolean open;

    /**
     * Constructor
     * @param first socket of the first entity
     * @param second socket of the second entity
     */
    public Tunnel(SecureSocket first, SecureSocket second) {
        this.first = first;
        this.second = second;
        startTransfer();
        open = true;
    }

    /**
     * Transfers data between the two entities
     */
    private void startTransfer() {
        thread = new Thread(new SecureTCPForwarder(first, second, true, false, this));
        thread2 = new Thread(new SecureTCPForwarder(second, first, false, true, this));
        thread.start();
        thread2.start();
    }

    /**
     * Closes the connection between the two entities
     */
    public synchronized void closeConnection() {
        if(open) {
            open = false;
            first.close();
            second.close();
            System.out.println("Tunnel closed.");
        }
    }
}
