import java.util.Arrays;
import java.security.SecureRandom;
import java.lang.Byte;
import java.nio.charset.StandardCharsets;
import java.nio.charset.Charset;
import java.util.Base64;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.io.ByteArrayOutputStream;
import java.security.SecureRandom;

/**
 * 128-bit AES in counter mode
 */
public class AES128_CTR {

    /**
    * Sbox used to mix bytes. Used to encrypt.
    */
    private static final int[][] box = {
        {0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76},
        {0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0},
        {0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15},
        {0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75},
        {0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84},
        {0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf},
        {0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8},
        {0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2},
        {0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73},
        {0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb},
        {0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79},
        {0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08},
        {0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a},
        {0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e},
        {0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf},
        {0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16}
    };

    /**
    * Inverse of the sbox. Used to decrypt.
    */
    private static final int[][] invBox = {
        {0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb},
        {0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb},
        {0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e},
        {0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25},
        {0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92},
        {0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84},
        {0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3, 0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06},
        {0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1, 0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b},
        {0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73},
        {0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e},
        {0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b},
        {0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4},
        {0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f},
        {0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef},
        {0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61},
        {0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d}
    };

    /**
    * Rijndael MixColumns.
    */
    private static final int[][] mixColumns = {
        {0x02, 0x03, 0x01, 0x01},
        {0x01, 0x02, 0x03, 0x01},
        {0x01, 0x01, 0x02, 0x03},
        {0x03, 0x01, 0x01, 0x02}
    };

    /**
    * Rijndael MixColumns inverse.
    */
    private static final int[][] invMixColumns = {
        {0x0e, 0x0b, 0x0d, 0x09},
        {0x09, 0x0e, 0x0b, 0x0d},
        {0x0d, 0x09, 0x0e, 0x0b},
        {0x0b, 0x0d, 0x09, 0x0e}
    };

    /**
    * Round constant.
    */
    private byte[] roundConstant = {(byte)0x01, (byte)0x02, (byte)0x04, (byte)0x08, (byte)0x10, (byte)0x20, (byte)0x40, (byte)0x80, (byte)0x1b, (byte)0x36};

    /**
    * Constructor.
    */
    public AES128_CTR() {}

    /**
    * Generates a secure random key.
    * @return keyBytes Secure random key.
    */
    public static byte[] generateKey() {
        SecureRandom random = new SecureRandom();
        byte[] keyBytes = new byte[16];
        random.nextBytes(keyBytes);
        return keyBytes;
    }

    /**
    * Generates a secure random counter of 64 bits.
    * @return secure Random counter of 64 bits.
    */
    public static BigInteger generateCTR() {
        SecureRandom random = new SecureRandom();
        return new BigInteger(128, random);
    }

    /**
    * Verifies if the length of the key is acceptable.
    * @param key Key to use to decrypt or encrypt.
    */
    private void verifyKeyLength(byte[] key) {
        if(key.length != 16) {
            throw new IllegalArgumentException("Invalid key");
        }
    }

    /**
    * Verifies if the length of the cipher is acceptable.
    * @param cipher Cipher to decrypt.
    */
    private void verifyCipherLength(byte[] cipher) {
        if(cipher.length % 16 != 0) {
            throw new IllegalArgumentException("Invalid ciphertext");
        }
    }

    /**
    * Retuns the last 16 bytes of the counter.
    * @param ctr Counter.
    * @return res Last 16 bytes of the counter.
    */
    private byte[] getLast16Bytes(BigInteger ctr) {
        byte[] ctrByte = ctr.toByteArray();
        byte[] res = new byte[16];
        System.arraycopy(ctrByte, 0, res, Math.max(16 - ctrByte.length, 0), Math.min(ctrByte.length, 16));
        return res;
    }

    /**
    * Prepares the encryption in AES128 counter mode.
    * @param message Message to encrypt.
    * @param  key Key to use to encrypt.
    * @param ctr Initial counter to use to encrypt.
    * @throws IllegalArgumentException The key is not equal to 16 bytes
    * @return Encrypted message.
    */
    public byte[] encrypt(byte[] message, byte[] key, BigIntegerPointer ctr) {
        verifyKeyLength(key);
        byte[] paddedMessage = addPadding(message);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        for(int i = 0; i < paddedMessage.length; i += 16) {
            ctr.ctr = ctr.ctr.add(BigInteger.ONE);
            byte[] ctrInBytes = getLast16Bytes(ctr.ctr);
            byte[][] ctrMatrixEncrypted = AESEncrypt(createMatrix(ctrInBytes), key);
            byte[][] ctrMatrixEncryptedXored = XOR(createMatrix(Arrays.copyOfRange(paddedMessage, i, i + 16)), ctrMatrixEncrypted);
            byte[] cipherBlock =  byteMatrixToBytes(AESEncrypt(ctrMatrixEncryptedXored, key));
            for(int byteInBlock = 0; byteInBlock < cipherBlock.length; byteInBlock++) {
                outputStream.write(cipherBlock[byteInBlock]);
            }
        }
        return outputStream.toByteArray();
    }

    /**
    * Prepares the decryption in AES128 counter mode.
    * @param cipher Cypher to decrypt.
    * @param key Key to use to decrypt.
    * @param ctr Initial counter to use to decrypt.
    * @throws IllegalArgumentException The key is not equal to 16 bytes
    * @throws IllegalArgumentException The number of bytes of the ciphertext is not a multiple of 16
    * @return Decrypted message.
    */
    public byte[] decrypt(byte[] cipher, byte[] key, BigIntegerPointer ctr) {
        verifyKeyLength(key);
        verifyCipherLength(cipher);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        for(int i = 0; i < cipher.length; i += 16) {
            ctr.ctr = ctr.ctr.add(BigInteger.ONE);
            byte[] ctrInBytes = getLast16Bytes(ctr.ctr);
            byte[][] ctrMatrixEncrypted = AESEncrypt(createMatrix(ctrInBytes), key);
            byte[][] decrypting = AESDecrypt(createMatrix(Arrays.copyOfRange(cipher, i, i + 16)), key);
            byte[][] messageDecrypted = XOR(decrypting, ctrMatrixEncrypted);
            byte[] plainBlock = byteMatrixToBytes(messageDecrypted);
            for(int byteInBlock = 0; byteInBlock < plainBlock.length; byteInBlock++) {
                outputStream.write(plainBlock[byteInBlock]);
            }
        }
        return removePadding(outputStream.toByteArray());
    }

    /**
    * Removes the padding from the output plain text
    * @param paddedPlainText The plain text in bytes
    * @throws IllegalArgumentException The last byte should be 0 or 1.
    * @throws IllegalArgumentException The last byte not equal to 1 after removing padding.
    * @return The plain text in bytes without any padding
    */
    private byte[] removePadding(byte[] paddedPlainText) {
        int lastByteIndex = paddedPlainText.length - 1;
        if (paddedPlainText[lastByteIndex] != 0 && paddedPlainText[lastByteIndex] != 1) {
            throw new IllegalArgumentException("Decryption error");
        }
        while(paddedPlainText[lastByteIndex] == 0) {
            lastByteIndex--;
        }
        if (paddedPlainText[lastByteIndex] != 1) {
            throw new IllegalArgumentException("Decryption error");
        }
        return Arrays.copyOfRange(paddedPlainText, 0, lastByteIndex);
    }

    /**
    * Convert a byte matrix into a byte array.
    * @param matrix The matrix to convert.
    * @return A byte array of the matrix
    */
    private byte[] byteMatrixToBytes(byte[][] matrix) {
        byte[] temp = new byte[matrix.length * matrix.length];
        for(int col = 0; col < matrix.length; col++) {
            for(int row = 0; row < matrix.length; row++) {
                temp[(col * 4) + row] = matrix[row][col];
            }
        }
        return temp;
    }

    /**
    * Algorithm which actually does the decryption in AES128 counter mode.
    * @param cipherMatrix Cipher text encoded in a matrix.
    * @param key Key used during the encryption.
    * @return Decrypted message in a matrix.
    */
    private byte[][] AESDecrypt(byte[][] cipherMatrix, byte[] key) {
        byte[][] keyMatrix = createMatrix(key);
        byte[][][] allRoundKeys = new byte[11][4][4];
        for(int i = 0; i < keyMatrix.length; i++) {
            for(int j = 0; j < keyMatrix.length; j++) {
                allRoundKeys[0][i][j] = keyMatrix[i][j];
            }
        }
        for(int round = 1; round <= 10; round++) {
            byte[][] temp = keyExpansion(allRoundKeys[round - 1], round);
            for(int i = 0; i < temp.length; i++) {
                for(int j = 0; j < temp.length; j++) {
                    allRoundKeys[round][i][j] = temp[i][j];
                }
            }
        }
        byte[][] xoredMatrix = XOR(cipherMatrix, allRoundKeys[10]);
        xoredMatrix = invShiftRow(xoredMatrix);
        xoredMatrix = substitution(xoredMatrix, true);
        for(int round = 9; round >= 1; round--) {
            xoredMatrix = XOR(xoredMatrix, allRoundKeys[round]);
            xoredMatrix = mixColumn(xoredMatrix, round, true);
            xoredMatrix = invShiftRow(xoredMatrix);
            xoredMatrix = substitution(xoredMatrix, true);
        }
        xoredMatrix = XOR(xoredMatrix, allRoundKeys[0]);
        return xoredMatrix;
    }

    /**
    * Algorithm which actually does the encryption in AES128 counter mode.
    * @param stateMatrix Message to encrypt encoded in a matrix.
    * @param key Key used to decrypt.
    * @return Encrypted message in a matrix.
    */
    private byte[][] AESEncrypt(byte[][] stateMatrix, byte[] key) {
        byte[][] keyMatrix = createMatrix(key);
        byte[][] xoredMatrix = XOR(stateMatrix, keyMatrix);
        byte[][] previousRoundKey = keyMatrix;
        byte[][] newRoundKey = new byte[4][4];
        for(int round = 1; round <= 9; round++) {
            newRoundKey = keyExpansion(previousRoundKey, round);
            xoredMatrix = substitution(xoredMatrix, false);
            xoredMatrix = shiftRow(xoredMatrix);
            xoredMatrix = mixColumn(xoredMatrix, round, false);
            xoredMatrix = XOR(xoredMatrix, newRoundKey);
            previousRoundKey = newRoundKey;
        }
        newRoundKey = keyExpansion(previousRoundKey, 10);
        xoredMatrix = substitution(xoredMatrix, false);
        xoredMatrix = shiftRow(xoredMatrix);
        xoredMatrix = XOR(xoredMatrix, newRoundKey);
        return xoredMatrix;
    }

    /**
    * Multiplies two bytes.
    * @param byte1 First byte to multiply.
    * @param byte2 Second byte to multiply.
    * @return Result of the multiplication.
    */
    private byte byteMulti(byte byte1, byte byte2) {
        byte res = 0;
        for (int counter = 0; counter < 8; counter++) {
            if ((byte2 & 1) != 0) {
                res ^= byte1;
            }
            boolean verif;
            if((byte1 & 0x80) != 0) {
                verif = true;
            }
            else {
                verif = false;
            }
            byte1 <<= 1;
            if (verif) {
                byte1 ^= 0x1B;
            }
            byte2 >>= 1;
        }
        return res;
    }

    /**
    * Performs the mix column for the Rijndeal algorithm. It performs a mixing of bytes to the message.
    * @param roundKey Key of a specific round.
    * @param round The standing round.
    * @param inv If it is for the encryption (inv = false) or the decryption (inv = true).
    * @return Modified message.
    */
    private byte[][] mixColumn(byte[][] roundKey, int round, boolean inv) {
        byte[] temp = new byte[4];
        for(int col = 0; col < roundKey.length; col++) {
            for(int row = 0; row < roundKey.length; row++) {
                temp[row] = roundKey[row][col];
            }
            if(!inv) {
                roundKey[0][col] = (byte) ( byteMulti(temp[0], ((byte) mixColumns[0][0])) ^ byteMulti(temp[1], ((byte) mixColumns[0][1])) ^ (temp[2]) ^ (temp[3]) );
                roundKey[1][col] = (byte) ( (temp[0]) ^ byteMulti(temp[1], ((byte) mixColumns[1][1])) ^ byteMulti(temp[2], ((byte) mixColumns[1][2])) ^ (temp[3]) );
                roundKey[2][col] = (byte) ( (temp[0]) ^ (temp[1]) ^ byteMulti(temp[2], ((byte) mixColumns[2][2])) ^ byteMulti(temp[3], ((byte) mixColumns[2][3])) );
                roundKey[3][col] = (byte) ( byteMulti(temp[0], ((byte) mixColumns[3][0])) ^ (temp[1]) ^ (temp[2]) ^ byteMulti(temp[3], ((byte) mixColumns[3][3])) );
            }
            else {
                roundKey[0][col] = (byte) ( byteMulti(temp[0], ((byte) invMixColumns[0][0])) ^ byteMulti(temp[1], ((byte) invMixColumns[0][1])) ^ byteMulti(temp[2], ((byte) invMixColumns[0][2])) ^ byteMulti(temp[3], ((byte) invMixColumns[0][3])) );
                roundKey[1][col] = (byte) ( byteMulti(temp[0], ((byte) invMixColumns[1][0])) ^ byteMulti(temp[1], ((byte) invMixColumns[1][1])) ^ byteMulti(temp[2], ((byte) invMixColumns[1][2])) ^ byteMulti(temp[3], ((byte) invMixColumns[1][3])) );
                roundKey[2][col] = (byte) ( byteMulti(temp[0], ((byte) invMixColumns[2][0])) ^ byteMulti(temp[1], ((byte) invMixColumns[2][1])) ^ byteMulti(temp[2], ((byte) invMixColumns[2][2])) ^ byteMulti(temp[3], ((byte) invMixColumns[2][3])) );
                roundKey[3][col] = (byte) ( byteMulti(temp[0], ((byte) invMixColumns[3][0])) ^ byteMulti(temp[1], ((byte) invMixColumns[3][1])) ^ byteMulti(temp[2], ((byte) invMixColumns[3][2])) ^ byteMulti(temp[3], ((byte) invMixColumns[3][3])) );
            }
        }
        return roundKey;
    }

    /**
    * Performs the Shift for the Rijndeal algorithm. It blurs the bytes of the message.
    * @param roundKey sKey of a specific round.
    * @return Modified message.
    */
    private byte[][] shiftRow(byte[][] roundKey) {
        roundKey[1] = shiftRow2(roundKey[1]);
        roundKey[2] = shiftRow3(roundKey[2]);
        roundKey[3] = shiftRow4(roundKey[3]);
        return roundKey;
    }

    /**
    * Performs the inverse shift of the key for the Rijndeal algorithm.
    * @param roundKey Key of a specific round.
    * @return Modified message.
    */
    private byte[][] invShiftRow(byte[][] roundKey) {
        roundKey[1] = invShiftRow2(roundKey[1]);
        roundKey[2] = shiftRow3(roundKey[2]);
        roundKey[3] = invShiftRow4(roundKey[3]);
        return roundKey;
    }

    /**
    * Performs the substitution for the Rijndeal algorithm. It matches the bytes of the message with the ssbox.
    * @param roundKey Key of a specific round.
    * @param inv If it is for the encryption (inv = false) or the decryption (inv = true).
    * @return Modified message.
    */
    private byte[][] substitution(byte[][] roundKey, boolean inv) {
        for(int row = 0; row < roundKey.length; row++) {
            for(int col = 0; col < roundKey[row].length; col++) {
                if(!inv) {
                    roundKey[row][col] = mapping(roundKey[row][col]);
                }
                else {
                    roundKey[row][col] = invMapping(roundKey[row][col]);
                }
            }
        }
        return roundKey;
    }


    /**
    * Performs the substitution for the Rijndeal algorithme. Modifies the key for each round to a new round key.
    * @param originalRoundKey Original key to modify.
    * @param round The standing round.
    * @return The new round key.
    */
    private byte[][] keyExpansion(byte[][] originalRoundKey, int round) {
        byte[][] roundKey = new byte[4][4];
        for(int i = 0; i < roundKey.length; i++) {
            for(int j = 0; j < roundKey.length; j++) {
                roundKey[i][j] = originalRoundKey[i][j];
            }
        }
        byte[] lastColumnRoundKey = swapLastColumn(roundKey);
        for(int i = 0; i < 4; i++) {
            lastColumnRoundKey[i] = mapping(lastColumnRoundKey[i]);
            if(i == 0) {
                lastColumnRoundKey[i] = (byte) (lastColumnRoundKey[i] ^ (roundConstant[round - 1]));
            }
        }
        for(int j = 0; j < 4; j++) {
            roundKey[j][0] = (byte) (roundKey[j][0] ^ lastColumnRoundKey[j]);
        }
        for(int k = 0; k < 4; k++) {
            for(int l = 1; l < 4; l++) {
                roundKey[k][l] = (byte) (roundKey[k][l] ^ roundKey[k][l - 1]);
            }
        }
        return roundKey;
    }

    /**
    * Shift for the second line of the matrix.
    * @param row Key of a specific round.
    * @return Modified message.
    */
    private byte[] shiftRow2(byte[] row) {
        byte[] shiftedRow = new byte[4];
        shiftedRow[0] = row[1];
        shiftedRow[1] = row[2];
        shiftedRow[2] = row[3];
        shiftedRow[3] = row[0];
        return shiftedRow;
    }

    /**
    * Performs the inverse shift for the second line of the matrix.
    * @param row Key of a specific round.
    * @return Modified message.
    */
    private byte[] invShiftRow2(byte[] row) {
        byte[] shiftedRow = new byte[4];
        shiftedRow[0] = row[3];
        shiftedRow[1] = row[0];
        shiftedRow[2] = row[1];
        shiftedRow[3] = row[2];
        return shiftedRow;
    }

    /**
    * Performs the shift for the third line of the matrix. Works for the inverse shift too.
    * @param row Key of a specific round.
    * @return Modified message.
    */
    private byte[] shiftRow3(byte[] row) {
        byte[] shiftedRow = new byte[4];
        shiftedRow[0] = row[2];
        shiftedRow[1] = row[3];
        shiftedRow[2] = row[0];
        shiftedRow[3] = row[1];
        return shiftedRow;
    }

    /**
    * Performs the shift for the last line of the matrix.
    * @param row Key of a specific round.
    * @return Modified message.
    */
    private byte[] shiftRow4(byte[] row) {
        byte[] shiftedRow = new byte[4];
        shiftedRow[0] = row[3];
        shiftedRow[1] = row[0];
        shiftedRow[2] = row[1];
        shiftedRow[3] = row[2];
        return shiftedRow;
    }

    /**
    * Performs the inverse shift for the last line of the matrix.
    * @param row Key of a specific round.
    * @return Modified message.
    */
    private byte[] invShiftRow4(byte[] row) {
        byte[] shiftedRow = new byte[4];
        shiftedRow[0] = row[1];
        shiftedRow[1] = row[2];
        shiftedRow[2] = row[3];
        shiftedRow[3] = row[0];
        return shiftedRow;
    }

    /**
    * Maps a byte for the ssbox.
    * @param byteToMap Byte to map.
    * @return Mapping result.
    */
    private byte mapping(byte byteToMap) {
        int i = byteToMap & 0xFF;
        String j = Integer.toHexString(i);
        if(j.length() < 2) {
            j = "0" + j;
        }
        return (byte)box[Integer.parseInt(String.valueOf(j.charAt(0)), 16)][Integer.parseInt(String.valueOf(j.charAt(1)), 16)];
    }

    /**
    * Inverse Mapping.
    * @param byteToInvMap Byte to map.
    * @return Mapping result.
    */
    private byte invMapping(byte byteToInvMap) {
        int i = byteToInvMap & 0xFF;
        String j = Integer.toHexString(i);
        if(j.length() < 2) {
            j = "0" + j;
        }
        return (byte)invBox[Integer.parseInt(String.valueOf(j.charAt(0)), 16)][Integer.parseInt(String.valueOf(j.charAt(1)), 16)];
    }

    /**
    * Modifies the last column during the key expansion.
    * @param roundKey Key of a specific round.
    * @return Swapped last column.
    */
    private byte[] swapLastColumn(byte[][] roundKey) {
        byte[] lastColumnRoundKey = new byte[4];
        lastColumnRoundKey[0] = roundKey[1][3];
        lastColumnRoundKey[1] = roundKey[2][3];
        lastColumnRoundKey[2] = roundKey[3][3];
        lastColumnRoundKey[3] = roundKey[0][3];
        return lastColumnRoundKey;
    }

    /**
    * Computes the XOR between two matrices.
    * @param matrix1 First matrix to XOR.
    * @param matrix2 Second matrix to XOR.
    * @return Result of the XOR between both matrices.
    */
    private byte[][] XOR(byte[][] matrix1, byte[][] matrix2) {
        byte[][] resMatrix = new byte[4][4];
        for(int i = 0; i < 4; i++) {
            for(int j = 0; j < 4; j++) {
                resMatrix[i][j] = (byte) (matrix1[i][j] ^ matrix2[i][j]);
            }
        }
        return resMatrix;
    }

    /**
    * Converts a column of bytes into a 4x4 matrix.
    * @param a16Bytes An array of byte.
    * @return A matrix of byte.
    */
    private byte[][] createMatrix(byte[] a16Bytes) {
        byte[][] matrix = new byte[4][4];
        for(int i = 0; i < 4; i++) {
            for(int j = 0; j < 4; j++) {
                matrix[j][i] = a16Bytes[(i * 4) + j];
            }
        }
        return matrix;
    }

    /**
    * Converts the message to a multiple of 16 bytes by adding a padding.
    * @param message Message to encrypt.
    * @return Modified message.
    */
    private byte[] addPadding(byte[] message) {
        int difference = message.length % 16;
        byte[] addedPaddingToMessage = new byte[message.length + (16 - difference)];
        System.arraycopy(message, 0, addedPaddingToMessage, 0, message.length);
        addedPaddingToMessage[message.length] = (byte)1;
        for(int i = message.length + 1; i < addedPaddingToMessage.length; i++) {
            addedPaddingToMessage[i] = (byte)0;
        }
        return addedPaddingToMessage;
    }
}
