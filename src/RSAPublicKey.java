import java.math.BigInteger;
import java.io.Serializable;

/**
 * Class representing a public RSA key.
 */
public class RSAPublicKey implements Serializable {

    /**
     * RSA modulus
     */
    private BigInteger _n;

    /**
     * RSA public exponent
     */
    private BigInteger _e;

    /**
     * Constructor
     *
     * @param n RSA modulus
     * @param e RSA public exponent
     */
    public RSAPublicKey(BigInteger n, BigInteger e) {
        _n = n;
        _e = e;
    }

    /**
     * Returns the RSA modulus
     *
     * @return the RSA modulus
     */
    public BigInteger getN() {
        return _n;
    }

    /**
     * Returns the RSA public exponent
     *
     * @return the RSA public exponent
     */
    public BigInteger getE() {
        return _e;
    }

}
