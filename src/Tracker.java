import java.io.IOException;
import java.net.Socket;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.net.InetSocketAddress;
import java.util.Random;

/**
 * Class representing the tracker
 */
public class Tracker extends PublicServer {

    /**
     * Request number corresponding to the adding of a peer to the tracker
     */
    public static final int ADDPEERSERVER = 1;

    /**
     * Request number corresponding to the waiting of peer to download a file
     */
    public static final int WANTDOWLOAD = 2;

    /**
     * Maps a filename and the list of its owners sockets
     */
    private Map<String, List<SecureSocket>> filesOwners;

    /**
    * Constructor
    * @param address address of the tracker
    * @throws IOException
    */
    public Tracker(InetSocketAddress address) throws IOException{
        super(address);
        filesOwners = new ConcurrentHashMap<String, List<SecureSocket>>();
    }

    /**
    * Adds a filename and it's owner's socket to the tracker
    * @param peer socket of the peer owning the file
    * @param filename name of the file owned by the peer
    */
    private synchronized void addFilePeer(SecureSocket peer, String filename) {
        if(filesOwners.containsKey(filename)) {
            filesOwners.get(filename).add(peer);
        }
        else {
            List<SecureSocket> temp = new ArrayList<SecureSocket>();
            temp.add(peer);
            filesOwners.put(filename, temp);
        }
        System.out.println("File added: " + filename);
    }

    /**
     * Returns the type of the request to the network. See Network class attributes.
     */
    protected byte getCaseNotify() {
        return Network.ADDTRACKER;
    }

    /**
     * Connects a new peer to the tracker and executes its request
     * @param requestSocket peer that made the request
     */
    protected void answerTheRequest(Socket requestSocket) {
        try {
            System.out.println("New connection of a peer");
            SecureSocket peerSocket = new SecureSocket(requestSocket);
            peerSocket.setRsa(getRSA());
            peerSocket.receiveAES128KeyCtr();
            int typeRequest = peerSocket.receiveInt();
            switch (typeRequest) {
            case ADDPEERSERVER:
                addPeer(peerSocket);
                break;
            case WANTDOWLOAD:
                sendAllAvailableFiles(peerSocket);
                try {
                    choiceFile(peerSocket);
                }
                catch(NullPointerException e) { // Selected file doesn't exist
                }
                peerSocket.close();
                break;
            default:
                peerSocket.close();
                System.out.println("Unknown request");
            }
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        catch(ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds a peer server and its files to the tracker given his socket
     * @param peerSocket socket of the peer server
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void addPeer(SecureSocket peerSocket) throws IOException, ClassNotFoundException {
        System.out.println ("Connection to a new peer server established");
        int nbrFile = peerSocket.receiveInt();
        for(int i = 0 ; i < nbrFile ; i++ ) {
            addFilePeer(peerSocket, peerSocket.receiveString());
        }
    }

    /**
     * Sends the list of available files to a a peer client
     * @param peerSocket socket of the peer client that requested the list of available files
     * @throws IOException
     */
    private void sendAllAvailableFiles(SecureSocket peerSocket) throws IOException{
        peerSocket.sendInt(filesOwners.size());
        System.out.println ("Sending the list of available files to a peer client.");
        for( String fileAvailable : filesOwners.keySet()) {
            peerSocket.sendString(fileAvailable);
        }
    }

    /**
     * Sets up a meeting between a peer server owning a file requested by a peer client and that peer client
     * @param peerSocket peer client socket
     * @throws IOException
     */
    private void choiceFile(SecureSocket peerSocket) throws IOException {
        String filename = peerSocket.receiveString();
        if(!filename.equals("")) {
            SecureSocket owner = getOwnerFile(filename);
            synchronized(owner) {
                System.out.println("File requested by a peer client: " + filename);
                owner.sendString(filename);// Let the two peer prepare the meeting
                owner.sendByte(peerSocket.receiveByte(true), true); // Transfer the key
                owner.sendByte(peerSocket.receiveByte(true), true); // Transfer the counter
                peerSocket.sendByte(owner.receiveByte(true), true); // Transfer the meeting point
                peerSocket.sendByte(owner.receiveByte(true), true); // Transfer the meeting key
            }
        }
    }

    /**
     * Returns the socket of one of the owner a given file
     * @param filename filename
     * @return socket of the file's owner
     */
    public SecureSocket getOwnerFile(String filename) {
        Random rand = new Random();
        List<SecureSocket> owners = filesOwners.get(filename);
        return owners.get(rand.nextInt(owners.size()));
    }
}
