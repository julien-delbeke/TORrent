import java.io.IOException;
import java.util.Scanner;
import java.net.InetSocketAddress;

/**
 * Class launching a server that is the network or an onion routing node.
 */
public class ServerLauncher {

    /**
     * Launches the server
     * @param argv server port, server address, network address
     */
    public static void main(String[] argv) {
        int port = 0;
        if(argv.length >= 2) {
            port = Integer.valueOf(argv[0]);
            InetSocketAddress address = new InetSocketAddress(argv[1], port);
            if(port == 8080) {
                try {
                    new Network(address).listen();
                }
                catch(IOException e) {
                }
            }
            else if(argv.length == 3) {
                NetworkAddress.setNetworkAddress(argv[2]);
                try {
                    new Node(address).listen();
                }
                catch (IOException a) {
                    a.printStackTrace();
                }
            }
            else {
                System.out.println("Argument error: You have forgot to provide the network hostname");
            }
        }
        else {
            System.out.println("Arguments error: The first argument is the port of the server. " +
                               "The second argument is the hostname of the server. " +
                               "The third argument is only needed for the creation of an onion routing node and is the network hostname");
        }
    }
}
