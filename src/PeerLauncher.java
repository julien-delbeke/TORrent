import java.util.Scanner;
import java.net.InetSocketAddress;
import java.io.IOException;

/**
 * Class launching the peers
 */
public class PeerLauncher {

    /**
     * Launches the peers
     * @param argv client directory, server directory and network address
     */
    public static void main(String[] argv) {
        try {
            if(argv.length == 3) {
                String clientDirectory = argv[0];
                String serverDirectory = argv[1];
                NetworkAddress.setNetworkAddress(argv[2]);
                new Peer(clientDirectory, serverDirectory).mainLoop();
            }
            else {
                System.out.println("Arguments error: The first argument is the destination directory of the downloaded files. " +
                                   "The second argument is the directory of the available files of the peer. " +
                                   "The third argument is the network hostname");
            }
        }
        catch (IOException a) {
            a.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
