import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.net.Socket;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;

/**
* Class setting up the onion routing
*/
public class OnionMaker {

    /**
     * RSAES-OAEP used for encryption
     */
    protected RSAES_OAEP rsa;

    /**
     * Contains the IP address and the RSA public key of the nodes in the onion path
     */
    private Map<InetSocketAddress, RSAPublicKey> nodesInformation;

    /**
     * Contains all the onion routing nodes on the network
     */
    private List<InetSocketAddress> nodes;

    /**
     * Address of the tracker
     */
    private InetSocketAddress trackerAddress;

    /**
     * RSA public key of the tracker
     */
    private RSAPublicKey trackerPublicKey;

    /**
     * Constructor
     * @throws IOException
     */
    public OnionMaker() throws IOException {
        rsa = new RSAES_OAEP();
        nodesInformation = new HashMap<InetSocketAddress, RSAPublicKey>();
        getNetworkInformation();
    }

    /**
     * Collects node addresses and their RSA keys
     * @throws IOException
     */
    private void getNetworkInformation() throws IOException{
        try {
            Socket network = NetworkAddress.getNetworkSocket();
            ObjectOutputStream networkOOS = new ObjectOutputStream(network.getOutputStream());
            ObjectInputStream networkOIS = new ObjectInputStream(network.getInputStream());
            networkOOS.writeByte(Network.GETINFORMATION);
            networkOOS.flush();
            int nbrNode = networkOIS.readInt();
            for(int i = 0 ; i < nbrNode ; i++ ) {
                InetSocketAddress address = (InetSocketAddress) networkOIS.readObject();
                RSAPublicKey key = (RSAPublicKey) networkOIS.readObject();
                nodesInformation.put(address, key);
            }
            trackerAddress = (InetSocketAddress) networkOIS.readObject();
            trackerPublicKey = (RSAPublicKey) networkOIS.readObject();
            networkOOS.writeByte(0);
            networkOOS.flush();
            nodes = new ArrayList<InetSocketAddress>();
            nodes.addAll(nodesInformation.keySet());
            networkOOS.close();
            networkOIS.close();
            network.close();
            System.out.println("All the information of the network has been retrieved.");
        }
        catch(ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates an onion path to a given destination with a given number of layers
     * @param dest address of the destination
     * @param numberLayer number of nodes in the onion path
     * @return socket of the first node in the onion path
     * @throws IOException
     */
    private SecureSocket createOnionRouting(InetSocketAddress dest, int numberLayer) throws IOException {
        Collections.shuffle(nodes);
        SecureSocket firstNode = new SecureSocket(nodes.get(0));
        firstNode.setRsa(rsa);
        InetSocketAddress layerToAdd = nodes.get(0);
        addLayerToOnion(layerToAdd, firstNode, Node.TUNNEL);
        for (int i = 1; i < numberLayer; i++) {
            layerToAdd = nodes.get(i);
            firstNode.sendObject(layerToAdd);
            addLayerToOnion(layerToAdd, firstNode, Node.TUNNEL);
        }
        firstNode.sendObject(dest);
        return firstNode;
    }

    /**
     * Adds a layer to the onion path
     * @param toAdd address of the next node in the onion path
     * @param first first node in the onion path
     * @param typeRequest usage of the node toAdd. See Node class attributes.
     * @throws IOException
     */
    private void addLayerToOnion(InetSocketAddress toAdd, SecureSocket first, byte typeRequest) throws IOException {
        byte[] key = AES128_CTR.generateKey();
        BigInteger ctr = AES128_CTR.generateCTR();
        first.sendRSAEncryption(concatenation(typeRequest, key, ctr.toByteArray()), nodesInformation.get(toAdd));
        first.addAES128KeyCtr(key, ctr);
    }

    /**
     * Creates a connection to the tracker through the onion path
     * @return firstNode first node in the onion path
     * @throws IOException
     */
    public SecureSocket connectTracker() throws IOException {
        SecureSocket firstNode;
        synchronized(this) {
            firstNode = createOnionRouting(trackerAddress, 3);
        }
        byte[] key = AES128_CTR.generateKey();
        BigInteger ctr = AES128_CTR.generateCTR();
        firstNode.sendRSAEncryption(concatenation(Node.TUNNEL, key, ctr.toByteArray()), trackerPublicKey); // Node.Tunnel has no importance because the tracker won't use it
        firstNode.addAES128KeyCtr(key, ctr);
        return firstNode;
    }

    /**
     * Joins a meeting in a given node with a given key.
     * @param dest node corresponding to the meeting point
     * @param meetingKey key allowing to join the meeting
     * @return firstNode first node in the onion path
     * @throws IOException
     */
    public SecureSocket joinMeeting(InetSocketAddress dest, byte[] meetingKey) throws IOException {
        SecureSocket firstNode;
        synchronized(this) {
            nodes.remove(dest); // To avoid using meeting point in the onion path
            firstNode = createOnionRouting(dest, 3);
            nodes.add(dest);
        }
        firstNode.sendRSAEncryption(concatenation(Node.JOINMEETING, meetingKey), nodesInformation.get(dest));
        return firstNode;
    }

    /**
     * Creates a meeting in a given node with a given key.
     * @param dest node corresponding to the meeting point
     * @param meetingKey key allowing to join the meeting
     * @return firstNode first node in the onion path
     * @throws IOException
     */
    public SecureSocket createMeeting(InetSocketAddress dest, byte[] meetingKey) throws IOException {
        SecureSocket firstNode;
        synchronized(this) {
            nodes.remove(dest); // To avoid using meeting point in the onion path
            firstNode = createOnionRouting(dest, 2); // The meeting point is the third node in the path
            nodes.add(dest);
        }
        addLayerToOnion(dest, firstNode, Node.CREATEMEETING);
        firstNode.sendByte(meetingKey, true);
        return firstNode;
    }

    /**
     * Returns a random node on the network
     * @return the random node
     */
    public synchronized InetSocketAddress getRandomNode() {
        Collections.shuffle(nodes);
        return nodes.get(0);
    }

    /**
     * Concatenates a byte and a byte array
     * @param a byte in the concatenation
     * @param b byte array in the concatenation
     * @return concat resulting byte array
     */
    private byte[] concatenation(byte a, byte[] b) {
        byte[] concat = new byte[b.length +  1];
        concat[0] = a;
        for(int i = 0 ; i < b.length; i++ ) {
            concat[i + 1] = b[i];
        }
        return concat;
    }

    /**
     * Concatenates a byte and a byte array
     * @param a byte in the concatenation
     * @param b first byte array in the concatenation
     * @param c second byte array in the concatenation
     * @return concat resulting byte array
     */
    private byte[] concatenation(byte a, byte[] b, byte[] c) {
        byte[] concat = new byte[b.length + c.length + 1];
        concat[0] = a;
        for(int i = 0 ; i < b.length; i++ ) {
            concat[i + 1] = b[i];
        }
        for(int i = 0 ; i < c.length; i++ ) {
            concat[i + 17] = c[i];
        }
        return concat;
    }
}
