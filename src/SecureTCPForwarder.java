import java.io.IOException;

/**
 * Class that implemenst a secure TCP forwarder.
 * Sends all the data coming from a first socket to a second socket.
 */
public class SecureTCPForwarder implements Runnable  {

    /**
     * Secure socket to receive information
     */
    private SecureSocket first;

    /**
     * Secure socket to send information
     */
    private SecureSocket second;

    /**
     * Indicates if a decryption is needed
     */
    boolean decrypt;

    /**
     * Indicates if an encryption is needed
     */
    boolean encrypt;

    /**
     * Tunnel through which the connection is established
     */
    private Tunnel tunnel;

    /**
     * Constructor
     * @param first source of the data
     * @param second destination of the data
     * @param decrypt indicates that incoming data should be decrypted
     * @param encrypt indicates that outgoing data should be encrypted
     * @param tunnel tunnel through which the connection is established
     */
    public SecureTCPForwarder(SecureSocket first, SecureSocket second, boolean decrypt, boolean encrypt, Tunnel tunnel) {
        this.first = first;
        this.second = second;
        this.decrypt = decrypt;
        this.encrypt = encrypt;
        this.tunnel = tunnel;
    }

    /**
     * Thread run method used to transfer all the data.
     */
    @Override
    public void run() {
        try {
            byte[] buffer = new byte[1];
            while (buffer != null && !Thread.interrupted()) {
                buffer = first.receiveByte(decrypt);
                if(buffer != null) {
                    second.sendByte(buffer, encrypt);
                }
            }
            tunnel.closeConnection();
        }
        catch(IOException e) {
            tunnel.closeConnection();
        }
    }
}
