import java.math.BigInteger;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;
import java.net.InetSocketAddress;

/**
 * Class that implements the client side of a peer
 */
public class PeerClient {

    /**
     * List of available files on the tracker that will be received by the peer
     */
    private List<String> files;

    /**
     * Scanner for input
     */
    private Scanner scanner;

    /**
     * Onion maker for the creation of an onion path
     */
    private OnionMaker onionMaker;

    /**
     * Destination directory of the downloaded files
     */
    private String directory;

    /**
     * Constructor
     * @param directory destination directory of the downloaded files
     * @param onionMaker used for the creation of an onion path
     * @throws IOException
     */
    public PeerClient(String directory, OnionMaker onionMaker) throws IOException {
        super();
        scanner = new Scanner(System.in);
        this.onionMaker = onionMaker;
        this.directory = directory;
    }

    /**
     * Files downloading main loop consisting of three steps.
     * Receiving list of available files, displaying list of available files and selecting a file to download.
     */
    public void mainLoop() {
        while(true) {
            try {
                System.out.println("Press enter to display the list of available files of the peers.");
                scanner.nextLine();
                SecureSocket socket = onionMaker.connectTracker();
                receiveListAvailableFiles(socket);
                showAvailableFile();
                System.out.println ("Select a file by entering its name or its number in the list. Return to the previous screen by pressing enter.");
                String userChoice = getValidFileSelection();
                String fileName = sendSelectedFilename(userChoice, socket);
                if("".equals(fileName)) {//no choice so quit
                    socket.close();
                }
                else {
                    receiveChosenFile(socket, fileName);
                }
            }
            catch(IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Returns a valid file selection that is a filename or the number of a file in the list of available files
     */
    private String getValidFileSelection() {
        String userChoice = scanner.nextLine();
        boolean valideChoice = false;
        while(!valideChoice) {
            try {
                while(!userChoice.equals("") && !files.contains(userChoice) && !(Integer.valueOf(userChoice) <= files.size())) {
                    System.out.println("Invalid number.");
                    userChoice = scanner.nextLine();
                }
                valideChoice = true;
            }
            catch(NumberFormatException e) {
                System.out.println("Invalid filename.");
                userChoice = scanner.nextLine();
            }
        }
        return userChoice;
    }

    /**
     * Receives the list of available files on the tracker
     * @param tracker the tracker
     * @throws IOException
     */
    private void receiveListAvailableFiles(SecureSocket tracker) throws IOException {
        tracker.sendInt(Tracker.WANTDOWLOAD);
        int nbrFile = tracker.receiveInt();
        files = new ArrayList<String>();
        for(int i = 0 ; i < nbrFile ; i++ ) {
            files.add(tracker.receiveString());
        }
    }

    /**
     * Displays the list of available files on the tracker
     */
    private void showAvailableFile() {
        System.out.println ("Available files of the peers:");
        for (int i = 0; i < files.size(); i++) {
            System.out.println(Integer.toString(i + 1) + ") " + files.get(i));
        }
    }

    /**
     * Sends filename selected by the user through a secure socket
     * @param userChoice file selection of the user
     * @param socket secure socket
     * @throws IOException
     */
    private String sendSelectedFilename(String userChoice, SecureSocket socket) throws IOException {
        String filename = "";
        if(!userChoice.equals("")) {
            if(files.contains(userChoice)) {
                filename = userChoice;
            }
            else {
                filename = files.get(Integer.valueOf(userChoice) - 1);
            }
        }
        socket.sendString(filename);
        return filename;
    }

    /**
     * Receives the selected file
     * @param socket secure socket through which the file is received
     * @param filename filename of the received file
     */
    private void receiveChosenFile(SecureSocket socket, String fileName) {
        try {
            byte[] sharedKey = AES128_CTR.generateKey();
            socket.sendByte(sharedKey, true);
            BigInteger ctr = AES128_CTR.generateCTR();
            socket.sendByte(ctr.toByteArray(), true);
            byte[] meetingKey = socket.receiveByte(true);
            InetSocketAddress meetingPoint = (InetSocketAddress) socket.receiveObject();
            System.out.println(meetingPoint);
            socket.close();
            socket = onionMaker.joinMeeting(meetingPoint, meetingKey);
            socket.addAES128KeyCtr(sharedKey, ctr);
            float sizeFile = socket.receiveInt();//to have a float divition
            int alreadyRead = 0;
            FileOutputStream fos = new FileOutputStream(directory + fileName);
            while(alreadyRead < sizeFile) {
                byte[] data = socket.receiveByte(true);
                fos.write(data, 0, data.length);
                alreadyRead += data.length;
                System.out.printf("Downloading... %.2f", (alreadyRead / sizeFile) * 100);
                System.out.println("%");
            }
            socket.close();
        }
        catch(ClassNotFoundException e) {
        }
        catch(IOException e) {
        }
    }
}
