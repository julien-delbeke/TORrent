import java.net.InetSocketAddress;
import java.net.Socket;
import java.io.IOException;

/**
 * Class representing the address of the network. Allows to avoid duplicating this information in all the classes and to change it easily.
 */
public class NetworkAddress {

    /**
     * Address of the network
     */
    private static InetSocketAddress networkAddress;

    /**
     * Returns a socket to communicate with the network
     * @return network socket
     * @throws IOException
     */
    public static Socket getNetworkSocket() throws IOException{
        return new Socket(networkAddress.getAddress(), networkAddress.getPort());
    }

    /**
     * Constructor
     * @param hostname network hostname
     */
    public static void setNetworkAddress(String hostname ) {
        networkAddress = new InetSocketAddress(hostname, 8080);
    }
}
