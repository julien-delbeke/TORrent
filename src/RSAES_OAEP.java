import java.math.BigInteger;
import java.security.SecureRandom;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * RSA with 1024-bit keys. We use OAEP-MGF1 padding with SHA-1 as its digest function.
 * We leave the optional "Label" parameter unset. (see ftp://ftp.rsasecurity.com/pub/pkcs/pkcs-1/pkcs-1v2-1.pdf)
 */
public class RSAES_OAEP {

    /**
     * Length in octets of the RSA modulus n
     */
    private final int K = 128;

    /**
     * The probability that a new BigInteger represents a prime number will exceed (1 - (1/2)^certainty)
     */
    private final int CERTAINTY = 10;

    /**
     * Output length in octets of the SHA1 hash function
     */
    private final int H_LEN = 20;

    /**
     * Hash value of the empty string.
     */
    private final byte[] L_HASH;

    /**
     * Source of randomness used to generate random BigInteger
     */
    private final SecureRandom randomness;

    /**
     * RSA public key
     */
    private RSAPublicKey _publicKey;

    /**
     * RSA private key
     */
    private BigInteger _d;

    /**
     * Constructor. Generates the keys and initializes the hash function.
     */
    public RSAES_OAEP() {
        randomness = new SecureRandom();
        BigInteger p = new BigInteger(bytesToBits(K) / 2, CERTAINTY, randomness);
        BigInteger q = new BigInteger(bytesToBits(K) / 2, CERTAINTY, randomness);

        BigInteger n = p.multiply(q);
        BigInteger phi = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));

        BigInteger e = new BigInteger(phi.bitLength(), randomness);
        while (e.compareTo(BigInteger.ONE) <= 0 || e.compareTo(phi) >= 0 || !e.gcd(phi).equals(BigInteger.ONE)) {
            e = new BigInteger(phi.bitLength(), randomness);
        }
        _publicKey = new RSAPublicKey(n, e);

        _d = e.modInverse(phi);

        byte[] emptyStringHash = null;
        try {
            emptyStringHash = MessageDigest.getInstance("SHA1").digest(new byte[0]);
        }
        catch(NoSuchAlgorithmException ex) {
        }
        L_HASH = emptyStringHash;

    }

    /**
     * Returns the RSA public key
     *
     * @return the RSA public key
     */
    public RSAPublicKey getPublicKey() {
        return _publicKey;
    }

    /**
    * Converts a number of bytes to a number of bits
    *
    * @param x number of bytes to be converted
    * @return corresponding number of bits
    */
    private int bytesToBits(int numOfBytes) {
        return numOfBytes * 8;
    }

    /**
    * Concatenates two byte arrays
    *
    * @param a first byte array in the concatenation
    * @param b second byte array in the concatenation
    * @return concat resulting byte array
    */
    private byte[] concatByteArrays(byte[] a, byte[] b) {
        byte[] concat = new byte[a.length + b.length];
        System.arraycopy(a, 0, concat, 0, a.length);
        System.arraycopy(b, 0, concat, a.length, b.length);
        return concat;
    }

    /**
    * Mask Generation Function based on a hash function
    *
    * @param mgfSeed seed from which mask is generated
    * @param maskLen intended length in octets of the mask
    * @return a byte array of length maskLen
    */
    private byte[] MGF1(byte[] mgfSeed, int maskLen) {
        if(maskLen > Math.pow(2, 32)*H_LEN) {
            throw new IllegalArgumentException("Mask too long");
        }

        byte[] t = new byte[0];
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA1");
            for(int counter = 0; counter < Math.ceil(maskLen / H_LEN) - 1; counter++) {
                t = concatByteArrays(t, sha1.digest(concatByteArrays(mgfSeed, i2os(BigInteger.valueOf(counter), 4))));
            }
            return Arrays.copyOfRange(t, 0, maskLen);
        }
        catch(NoSuchAlgorithmException e) {
            return null;
        }
    }

    /**
    * XOR's two byte arrays
    *
    * @param a first operand
    * @param b second operand
    * @return xor resulting byte array
    */
    private byte[] xorByteArrays(byte[] a, byte[] b) {
        byte[] smallestArray = null, biggestArray = null;
        if(a.length <= b.length) {
            smallestArray = a;
            biggestArray = b;
        }
        else {
            smallestArray = b;
            biggestArray = a;
        }

        byte[] xor = Arrays.copyOfRange(biggestArray, 0, biggestArray.length);
        for(int i = 0; i < smallestArray.length; i++ ) {
            xor[i] = (byte)(a[i] ^ b[i]);
        }
        return xor;
    }

    /**
    * Length checking of the encryption operation
    *
    * @param mLen length of the message to be encrypted
    */
    private void encryptLengthChecking(int mLen) {
        if(mLen > K - 2 * H_LEN - 2) {
            throw new IllegalArgumentException("Message too long");
        }
    }

    /**
    * Encodes a message using the EME-OAEP encoding method
    *
    * @param message message to be encoded
    * @return the encoded message
    */
    private byte[] EME_OAEPEncoding(byte[] message) {
        int mLen = message.length;
        byte[] ps = new byte[K - mLen - 2 * H_LEN - 2]; // Generate an octet string PS consisting of K – mLen – 2hLen – 2 zero octets
        byte[] db = concatByteArrays(concatByteArrays(concatByteArrays(L_HASH, ps), new byte[] {1}), message);
        byte[] seed = i2os(new BigInteger(H_LEN * 8, randomness), H_LEN);
        byte[] dbMask = MGF1(seed, K - H_LEN - 1);
        byte[] maskedDb = xorByteArrays(db, dbMask);
        byte[] seedMask = MGF1(maskedDb, H_LEN);
        byte[] maskedSeed = xorByteArrays(seed, seedMask);
        return concatByteArrays(concatByteArrays(new byte[] {0}, maskedSeed), maskedDb);
    }

    /**
    * Encrypts a message using the RSA algorithm
    *
    * @param message message to be encrypted
    * @return the encrypted message
    */
    private byte[] RSAEncryption(byte[] em, RSAPublicKey publicKey) {
        BigInteger m = os2i(em);
        BigInteger c = m.modPow(publicKey.getE(), publicKey.getN());
        return i2os(c, K);
    }

    /**
    * Encrypts a message with a given RSA public key
    *
    * @param message message to be encrypted
    * @param publicKey RSA public key to use
    * @return the encrypted message
    */
    public byte[] encrypt(byte[] message, RSAPublicKey publicKey) {
        encryptLengthChecking(message.length);
        byte[] em = EME_OAEPEncoding(message);
        return RSAEncryption(em, publicKey);
    }

    /**
    * Encrypts a message
    *
    * @param message message to be encrypt
    * @return the encrypted message
    */
    public byte[] encrypt(byte[] message) {
        return encrypt(message, _publicKey);
    }

    /**
    * Length checking of the decryption operation
    *
    * @param cLen length of the ciphertext to be encrypted
    */
    private void decryptLengthChecking(int cLen) {
        if (cLen != K || K < 2 * H_LEN + 2) {
            throw new IllegalArgumentException("Decryption error");
        }
    }

    /**
    * Decrypts a ciphertext using the RSA algorithm
    *
    * @param ciphertext ciphertext to be decrypted
    * @return the decrypted ciphertext
    */
    private byte[] RSADecryption(byte[] ciphertext) {
        BigInteger c = os2i(ciphertext);
        BigInteger m = c.modPow(_d, _publicKey.getN());
        return i2os(m, K);
    }

    /**
    * Decodes a message using the EME-OAEP decoding method
    *
    * @param message message to be decoded
    * @return the decoded message
    */
    private byte[] EME_OAEPDecoding(byte[] em) {
        byte[] y = Arrays.copyOfRange(em, 0, 1);
        if (y[0] != 0) {
            throw new IllegalArgumentException("Decryption error");
        }
        byte[] maskedSeed = Arrays.copyOfRange(em, 1, H_LEN + 1);
        byte[] maskedDb = Arrays.copyOfRange(em, H_LEN + 1, em.length);
        byte[] seedMask = MGF1(maskedDb, H_LEN);
        byte[] seed = xorByteArrays(maskedSeed, seedMask);
        byte[] dbMask = MGF1(seed, K - H_LEN - 1);
        byte[] db = xorByteArrays(maskedDb, dbMask);
        byte[] lHashPrime = Arrays.copyOfRange(db, 0, H_LEN);
        if (!Arrays.equals(L_HASH, lHashPrime)) {
            throw new IllegalArgumentException("Decryption error");
        }
        int separatorIndex = H_LEN;
        while(db[separatorIndex] == 0) {
            separatorIndex++;
        }
        if (db[separatorIndex] != 1) {
            throw new IllegalArgumentException("Decryption error");
        }
        return Arrays.copyOfRange(db, separatorIndex + 1, db.length);
    }

    /**
    * Decrypts a ciphertext
    *
    * @param ciphertext ciphertext to be decrypted
    * @return the decrypted message
    */
    public byte[] decrypt(byte[] ciphertext) {
        decryptLengthChecking(ciphertext.length);
        byte[] em = RSADecryption(ciphertext);
        return EME_OAEPDecoding(em);
    }

    /**
    * Converts a nonnegative integer to an octet string of a specified length.
    *
    * @param x nonnegative integer to be converted
    * @param xLen intended length of the resulting octet string
    * @return corresponding octet string of length xLen
    */
    private byte[] i2os(final BigInteger x, final int xLen) {
        if(x == null || x.signum() == -1) {
            throw new IllegalArgumentException("Integer should be a positive number or 0");
        }

        if(xLen < 1) {
            throw new IllegalArgumentException("Size of the octet string should be at least 1 but is " + xLen);
        }

        if(x.compareTo(new BigInteger("256", 10).pow(xLen)) >= 0) {
            throw new IllegalArgumentException("Integer too large");
        }

        byte[] res;
        final byte[] signed = x.toByteArray();
        if(signed.length == xLen) {
            res = signed;
        }
        else if(signed.length < xLen) {
            res = new byte[xLen];
            System.arraycopy(signed, 0, res, xLen - signed.length, signed.length);
        }
        else if(signed.length == xLen + 1 && signed[0] == 0x00) {
            res = new byte[xLen];
            System.arraycopy(signed, 1, res, 0, xLen);
        }
        else {
            throw new IllegalArgumentException("Integer does not fit into an array of size " + xLen);
        }
        return res;
    }

    /**
    * Converts an octet string of size K to a nonnegative integer.
    *
    * @param x octet string to be converted
    * @return corresponding nonnegative integer
    */
    private BigInteger os2i(final byte[] x) {
        if(x.length != K) {
            throw new IllegalArgumentException("Size of the octet string should be precisely " + K);
        }
        return new BigInteger(1, x);
    }
}
